package com.atguigu.gulimall.auth.constant;

/**
 * @author zehao
 * @create 2022-12-23 15:46
 */
public class AuthServerConstant {

    public static final String SMS_CODE_CACHE_PREFIX = "sms:code:";

}
