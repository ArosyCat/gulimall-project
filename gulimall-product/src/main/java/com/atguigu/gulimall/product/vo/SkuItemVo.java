package com.atguigu.gulimall.product.vo;

import com.atguigu.gulimall.product.entity.SkuImagesEntity;
import com.atguigu.gulimall.product.entity.SkuInfoEntity;
import com.atguigu.gulimall.product.entity.SpuInfoDescEntity;
import lombok.Data;

import java.util.List;

/**
 * @author zehao
 * @create 2022-12-19 17:26
 */
@Data
public class SkuItemVo {

    /**
     * sku基本信息  sku_pms
     */
    private SkuInfoEntity skuInfoEntity;

    /**
     * 有无货
     */
    private Boolean hasStock = true;

    /**
     * sku的图片信息  sku_images
     */
    private List<SkuImagesEntity> images;

    /**
     * 获取spu的销售属性组合
     */
    private List<SkuItemSaleAttrVo> saleAttrs;

    /**
     * 获取spu的介绍
     */
    private SpuInfoDescEntity desc;

    /**
     * 获取spu规格参数信息
     */
    private List<SpuItemAttrGroupVo> groupAttrs;

    @Data
    public static class SkuItemSaleAttrVo {
        private Long attrId;
        private String attrName;
        private List<AttrValueWithSkuIdVo> attrValues;
    }

    @Data
    public static class SpuItemAttrGroupVo {
        private String groupName;
        private List<SpuBaseAttrVo> baseAttrs;
    }

    @Data
    public static class SpuBaseAttrVo {
        private String attrName;
        private List<String> attrValues;
    }

}
