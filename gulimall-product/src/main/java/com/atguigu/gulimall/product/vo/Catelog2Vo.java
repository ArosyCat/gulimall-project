package com.atguigu.gulimall.product.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author zehao
 * @create 2022-12-12 11:12
 * 2级分类vo
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
public class Catelog2Vo {

    private String catalog1Id; // 1级父分类id

    private List<Catalog3Vo> catalog3List; // 3级子分类

    private String id;

    private String name;

    /**
     * 3级分类vo
     */
    @AllArgsConstructor
    @NoArgsConstructor
    @Data
    public static class Catalog3Vo {
        private String catalog2Id;
        private String id;
        private String name;
    }

}
