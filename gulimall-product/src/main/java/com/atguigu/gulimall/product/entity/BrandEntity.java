package com.atguigu.gulimall.product.entity;

import com.atguigu.common.valid.MyValidAnnotation;
import com.atguigu.common.valid.SaveGroup;
import com.atguigu.common.valid.UpdateGroup;
import com.atguigu.common.valid.UpdateStatusGroup;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;
import org.hibernate.validator.constraints.URL;

import javax.validation.constraints.*;

/**
 * 品牌
 * 
 * @author zehao
 * @email 374964794@qq.com
 * @date 2021-12-18 15:01:31
 */
@Data
@TableName("pms_brand")
public class BrandEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 品牌id
	 */
	@TableId
	@NotNull(message = "品牌Id不能为空", groups = {UpdateGroup.class,UpdateStatusGroup.class})
	@Null(message = "多余的品牌Id", groups = {SaveGroup.class})
	private Long brandId;
	/**
	 * 品牌名
	 */
	@NotBlank(message = "品牌名不能为空", groups = {SaveGroup.class, UpdateGroup.class})
	private String name;
	/**
	 * 品牌logo地址
	 */
//	@NotEmpty(message = "品牌logo不能为空", groups = {SaveGroup.class})
//	@URL(message = "品牌logo要求是URL", groups = {SaveGroup.class, UpdateGroup.class})
	private String logo;
	/**
	 * 介绍
	 */
	@NotEmpty(message = "介绍不能为空", groups = {SaveGroup.class})
	private String descript;
	/**
	 * 显示状态[0-不显示；1-显示]
	 */
	@NotNull(message = "显示状态值不能为空", groups = {SaveGroup.class})
	@MyValidAnnotation(regexp = "^[0,1]$", groups = {SaveGroup.class, UpdateGroup.class,UpdateStatusGroup.class}, message = "显示状态值要求为0或1")
	private Integer showStatus;
	/**
	 * 检索首字母
	 */
	@NotEmpty(message = "检索首字母不能为空", groups = {SaveGroup.class})
	@Pattern(regexp = "^[a-zA-Z]$", message = "检索首字母必须是 a-z 或者 A-Z", groups = {SaveGroup.class, UpdateGroup.class})
	private String firstLetter;
	/**
	 * 排序
	 */
	@NotNull(message = "排序值不能为空", groups = {SaveGroup.class})
	@Min(value = 0, message = "排序值不能小于0", groups = {SaveGroup.class, UpdateGroup.class})
	private Integer sort;

}
