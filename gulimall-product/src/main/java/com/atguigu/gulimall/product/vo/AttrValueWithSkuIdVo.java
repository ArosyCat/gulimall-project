package com.atguigu.gulimall.product.vo;

import lombok.Data;

/**
 * @author zehao
 * @create 2022-12-21 16:14
 */
@Data
public class AttrValueWithSkuIdVo {

    private String attrValue;
    private String skuIds;

}
