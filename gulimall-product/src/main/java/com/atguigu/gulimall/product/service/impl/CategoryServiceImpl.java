package com.atguigu.gulimall.product.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.atguigu.gulimall.product.service.CategoryBrandRelationService;
import com.atguigu.gulimall.product.vo.Catelog2Vo;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.script.DefaultRedisScript;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.atguigu.common.utils.PageUtils;
import com.atguigu.common.utils.Query;

import com.atguigu.gulimall.product.dao.CategoryDao;
import com.atguigu.gulimall.product.entity.CategoryEntity;
import com.atguigu.gulimall.product.service.CategoryService;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import javax.cache.annotation.CacheValue;


@Service("categoryService")
public class CategoryServiceImpl extends ServiceImpl<CategoryDao, CategoryEntity> implements CategoryService {
    /**
     * 简单缓存，本地缓存
     * 存在问题：同属于一个服务(或者一台机器)，在分布式情况下，多个相同的项目比如A,B,C
     * 第一次请求被nginx负载均衡到 A：
     * 本地缓存中查询不到 -> 数据库查询 -> 放入A的本地缓存
     * 第二次请求被nginx负载均衡到 B:
     * 本地缓存还是查询不到 -> 数据库查询 -> 放入B的本地缓存
     * 第三次请求被nginx负载均衡到 C:
     * 本地缓存依旧查询不到 -> 数据库查询 -> 放入C的本地缓存....
     * 还有修改了数据的情况，数据一致性没有保证，所以分布式系统需要使用缓存中间件
     */
//    private Map<String, Object> cache = new HashMap<>();

    @Autowired
    private StringRedisTemplate redisTemplate;

    @Autowired
    private CategoryBrandRelationService categoryBrandRelationService;

    @Autowired
    private RedissonClient redisson;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<CategoryEntity> page = this.page(
                new Query<CategoryEntity>().getPage(params),
                new QueryWrapper<CategoryEntity>()
        );

        return new PageUtils(page);
    }

    /**
     * 获得分类的树形结构
     * @return
     */
    @Override
    public List<CategoryEntity> getListTree() {
        List<CategoryEntity> categoryEntities = baseMapper.selectList(null);
        List<CategoryEntity> listTree = categoryEntities.stream()
                .filter(categoryEntity -> categoryEntity.getCatLevel() == 1)
                .map(categoryEntity -> {
                    categoryEntity.setChildrenCategoryList(getChildrenList(categoryEntity, categoryEntities));
                    return categoryEntity;
                })
                .sorted(Comparator.comparing(CategoryEntity::getSort))
                .collect(Collectors.toList());
        return listTree;
    }

    @Override
    public void removeMenuByIds(List<Long> idList) {
        // 待处理
        baseMapper.deleteBatchIds(idList);
    }

    /**
     * 找到catelog的完整路径
     * @param catelogId
     * @return
     */
    @Override
    public Long[] findCatelogPath(Long catelogId) {
        List<Long> paths = new ArrayList<>();
        this.recurGetPath(paths, catelogId);
        return paths.toArray(new Long[paths.size()]);
    }

    /**
     * @CacheEvict 失效模式
     * @param category
     */
//    @CacheEvict(value = "category", key = "'getLevel1Categorys'")
//    @Caching(evict = {
//            @CacheEvict(value = "category", key = "'getLevel1Categorys'"),
//            @CacheEvict(value = "category", key = "'getCatalogJson'")
//    })
    @CacheEvict(value = "category", allEntries = true)
    @Transactional
    @Override
    public void updateCascade(CategoryEntity category) {
        this.updateById(category);
        // 级联更新其他关联数据
        if (!StringUtils.isEmpty(category.getName())) {
            categoryBrandRelationService.updateCategory(category.getCatId(), category.getName());
        }
        // 同时修改缓存中的数据
    }

    /**
     * 1. 每一个需要缓存的数据我们都来指定要放到哪一个名字的缓存【缓存的分区，按照业务类型分】
     * 2. @Cacheable ({"category"})
     *      代表当前方法的结果需要缓存，如果缓存中有，方法不用调用，如果缓存中没有，会调用方法，最后将方法的结果放入缓存
     * 3. 默认行为
     *      1、如果缓存中有，方法不用调用
     *      2、key默认自动生成，缓存的名字 category::SimpleKey []
     *      3、缓存的value的值，默认使用jdk序列化机制，将序列化猴的数据存到redis
     *      4、默认ttl时间 -1
     * 4. 自定义
     *      1、指定生成的缓存使用的key     key属性指定，接收一个Spel表达式
     *      2、指定缓存的数据的存活时间
     *      3、将数据保存为json格式
     *          CacheAutoConfiguration
     *          RedisCacheConfiguration
     */
    @Override
    @Cacheable(value = {"category"}, key = "#root.method.name",sync = true)
    public List<CategoryEntity> getLevel1Categorys() {
        System.out.println("调用getLevel1Categorys。。。");
        List<CategoryEntity> categoryEntities = this.baseMapper.selectList(new QueryWrapper<CategoryEntity>().eq("parent_cid", 0));
        return categoryEntities;
    }

    @Cacheable(value = "category", key = "#root.methodName",sync = true)
    @Override
    public Map<String, List<Catelog2Vo>> getCatalogJson() {
        System.out.println("调用getCatalogJson。。。");
        List<CategoryEntity> selectList = baseMapper.selectList(null);
        List<CategoryEntity> level1Categorys = this.getParentCid(selectList, 0L);
        Map<String, List<Catelog2Vo>> collect = level1Categorys.stream().collect(Collectors.toMap(key -> key.getCatId().toString(), value -> {
            List<CategoryEntity> categoryEntities = this.getParentCid(selectList, value.getCatId());
            List<Catelog2Vo> catelog2VoList = null;
            if (categoryEntities != null) {
                catelog2VoList = categoryEntities.stream().map(l2 -> {
                    Catelog2Vo catelog2Vo = new Catelog2Vo(value.getCatId().toString(), null, l2.getCatId().toString(), l2.getName());
                    List<CategoryEntity> entityList = this.getParentCid(selectList, l2.getCatId());
                    if (entityList != null) {
                        List<Catelog2Vo.Catalog3Vo> catalog3VoList = entityList.stream().map(l3 -> {
                            Catelog2Vo.Catalog3Vo catalog3Vo = new Catelog2Vo.Catalog3Vo(l2.getCatId().toString(), l3.getCatId().toString(), l3.getName());
                            return catalog3Vo;
                        }).collect(Collectors.toList());
                        catelog2Vo.setCatalog3List(catalog3VoList);
                    }
                    return catelog2Vo;
                }).collect(Collectors.toList());
            }
            return catelog2VoList;
        }));
        return collect;
    }


    /**
     * 使用缓存容易出现：OutOfDirectMemoryError 堆外内存溢出
     * 原因：
     * 1. springboot2.0 默认使用lettuce作为操作redis的客户端，他使用netty进行网络通信
     * 2. lettuce的bug导致堆外内存溢出 -Xmx300m：netty如果没有指定堆外内存，默认使用-Xmx300m
     *    可以通过 -Dio.netty.maxDirectMemory进行设置
     * 解决方案：不能使用 -Dio.netty.maxDirectMemory 只去调大堆外内存
     * 1. 升级lettuce客户端
     * 2. 切换使用 jedis
     * @return
     */
//    @Override
//    public Map<String, List<Catelog2Vo>> getCatalogJson() {
//
//        /**
//         * 缓存问题：
//         * 1. 空结果缓存，解决缓存穿透
//         * 2. 设置随机的过期时间，解决缓存雪崩
//         * 3. 加锁，解决缓存击穿
//         */
//
//        // 给缓存中放json字符串，拿出的json字符串还需要逆转为能用的对象类型，这个就是【序列化与反序列化】
//        // 加入缓存，缓存中存的数据是json字符串
//        // 存json的好处：JSON跨语言，跨平台兼容
//        String catalogJson = redisTemplate.opsForValue().get("catalogJson");
//        if (StringUtils.isEmpty(catalogJson)) {
//            // 缓存中没有，查询数据库
//            System.out.println("缓存不命中。。。。。查询数据库。。。");
//            Map<String, List<Catelog2Vo>> collect = this.getCatalogJsonFromDBWithRedisson();
//            return collect;
//        }
//        System.out.println("缓存命中。。。。直接返回。。。。");
//        // 转为我们指定的对象
//        Map<String, List<Catelog2Vo>> result = JSON.parseObject(catalogJson, new TypeReference<Map<String, List<Catelog2Vo>>>(){});
//        return result;
//    }


    /**
     * 从数据库查询并封装分类数据，redisson锁
     * 缓存数据一致性
     * 1. 双写模式
     * 2. 失效模式
     */
    public Map<String, List<Catelog2Vo>> getCatalogJsonFromDBWithRedisson() {
        // 1. 占分布式锁，锁的名字
        // 锁的粒度：具体缓存的是某个数据，11-号商品： product-11-lock
        RLock redissonLock = redisson.getLock("catalogJson-lock");
        redissonLock.lock(30, TimeUnit.SECONDS);
        Map<String, List<Catelog2Vo>> dataFromDB;
        try {
            dataFromDB = getDataFromDB();
        } finally {
            redissonLock.unlock();
        }
        return dataFromDB;
    }

    /**
     * 从数据库查询并封装分类数据
     */
//    public Map<String, List<Catelog2Vo>> getCatalogJsonFromDBWithRedisLock() {
//        // 1. 占分布式锁，抢占redis
//        // 在这里抢占加锁的同时也一起加上过期时间，避免出现特殊情况导致死锁
//
//        String uuid = UUID.randomUUID().toString();
//        Boolean lock = redisTemplate.opsForValue().setIfAbsent("lock", uuid, 300, TimeUnit.SECONDS);
//        if (lock) {
//            System.out.println("获取分布式锁成功。。。");
//            // 2. 设置过期时间，30秒，但是不能在这里设置，必须和加锁是同步的，原子的
////            redisTemplate.expire("lock",30, TimeUnit.SECONDS);
//            Map<String, List<Catelog2Vo>> dataFromDB;
//            try {
//                dataFromDB = getDataFromDB();
//            } finally {
//                String script = "if redis.call('get', KEYS[1]) == ARGV[1] then return redis.call('del', KEYS[1]) else return 0 end";
//                Long lock1 = redisTemplate.execute(new DefaultRedisScript<Long>(script, Long.class), Arrays.asList("lock"),uuid);
//            }
//            // 业务执行结束，解锁(删除锁)
//            // 这里删锁的时候要知道删除的是自己的锁，不然可能删了其他线程的锁，出现查询为null的问题
////            redisTemplate.delete("lock");
////             获取值对比、对比成功删除，这两步也必须是一个原子操作
////            String lockValue = redisTemplate.opsForValue().get("lock");
////            if (uuid.equals(lockValue)) {
////                // 删除自己的锁
////                redisTemplate.delete("lock");
////            }
//            // 加上 Lua脚本来解锁（删除锁）
//            return dataFromDB;
//        } else {
//            System.out.println("获取分布式锁失败。。。等待重试。。。");
//            // 加锁失败，重试
//            // 休眠100ms
//            try {
//                Thread.sleep(200);
//            } catch (Exception e) {
//                throw new RuntimeException(e);
//            }
//            return getCatalogJsonFromDBWithRedisLock(); // 自旋的方式获取锁
//        }
//    }


    public Map<String, List<Catelog2Vo>> getDataFromDB() {
        String catalogJson = redisTemplate.opsForValue().get("catalogJson");
        if (!StringUtils.isEmpty(catalogJson)) {
            Map<String, List<Catelog2Vo>> result = JSON.parseObject(catalogJson, new TypeReference<Map<String, List<Catelog2Vo>>>(){});
            return result;
        }
        System.out.println("查询了数据库。。。。");
        List<CategoryEntity> selectList = baseMapper.selectList(null);
        List<CategoryEntity> level1Categorys = this.getParentCid(selectList, 0L);
        Map<String, List<Catelog2Vo>> collect = level1Categorys.stream().collect(Collectors.toMap(key -> key.getCatId().toString(), value -> {
            List<CategoryEntity> categoryEntities = this.getParentCid(selectList, value.getCatId());
            List<Catelog2Vo> catelog2VoList = null;
            if (categoryEntities != null) {
                catelog2VoList = categoryEntities.stream().map(l2 -> {
                    Catelog2Vo catelog2Vo = new Catelog2Vo(value.getCatId().toString(), null, l2.getCatId().toString(), l2.getName());
                    List<CategoryEntity> entityList = this.getParentCid(selectList, l2.getCatId());
                    if (entityList != null) {
                        List<Catelog2Vo.Catalog3Vo> catalog3VoList = entityList.stream().map(l3 -> {
                            Catelog2Vo.Catalog3Vo catalog3Vo = new Catelog2Vo.Catalog3Vo(l2.getCatId().toString(), l3.getCatId().toString(), l3.getName());
                            return catalog3Vo;
                        }).collect(Collectors.toList());
                        catelog2Vo.setCatalog3List(catalog3VoList);
                    }
                    return catelog2Vo;
                }).collect(Collectors.toList());
            }
            return catelog2VoList;
        }));
        String jsonString = JSON.toJSONString(collect);
        redisTemplate.opsForValue().set("catalogJson", jsonString);
        return collect;
    }

    /**
     * 从数据库查询并封装分类数据
     */
    public Map<String, List<Catelog2Vo>> getCatalogJsonFromDBWithLocalLock() {
        // 从缓存中获取
//        Map<String, List<Catelog2Vo>> catalogJson = (Map<String, List<Catelog2Vo>>) cache.get("catalogJson");
//        if (catalogJson == null) {
//
//            // 放入缓存
//            cache.put("catalogJson", collect);
//        }
        // 只要是同一把锁，就能锁住，需要的这个锁的所有线程
        // 1. this：SpringBoot所有的组件在容器中都是单例的。
        //      但是不适用分布式的环境，分布式中每个服务都独立一个ioc容器
        // 2. 在分布式情况下，想要锁住所有，需要使用分布式锁
        synchronized (redisTemplate) {
            // 得到锁以后，我们应该再去缓存中确定一次，如果没有再查询
            String catalogJson = redisTemplate.opsForValue().get("catalogJson");
            if (!StringUtils.isEmpty(catalogJson)) {
                // 缓存不为null，直接返回
                Map<String, List<Catelog2Vo>> result = JSON.parseObject(catalogJson, new TypeReference<Map<String, List<Catelog2Vo>>>(){});
                return result;
            }
            System.out.println("查询了数据库。。。。");
            // 1. 将数据库的多次查询变为一次
            List<CategoryEntity> selectList = baseMapper.selectList(null);
            List<CategoryEntity> level1Categorys = this.getParentCid(selectList, 0L);
            // 2. 封装数据
            Map<String, List<Catelog2Vo>> collect = level1Categorys.stream().collect(Collectors.toMap(key -> key.getCatId().toString(), value -> {
                // 查到这个一级分类的二级分类
                List<CategoryEntity> categoryEntities = this.getParentCid(selectList, value.getCatId());
                List<Catelog2Vo> catelog2VoList = null;
                if (categoryEntities != null) {
                    catelog2VoList = categoryEntities.stream().map(l2 -> {
                        Catelog2Vo catelog2Vo = new Catelog2Vo(value.getCatId().toString(), null, l2.getCatId().toString(), l2.getName());
                        // 找当前分类的三级分类封装成vo
                        List<CategoryEntity> entityList = this.getParentCid(selectList, l2.getCatId());
                        if (entityList != null) {
                            List<Catelog2Vo.Catalog3Vo> catalog3VoList = entityList.stream().map(l3 -> {
                                Catelog2Vo.Catalog3Vo catalog3Vo = new Catelog2Vo.Catalog3Vo(l2.getCatId().toString(), l3.getCatId().toString(), l3.getName());
                                return catalog3Vo;
                            }).collect(Collectors.toList());
                            catelog2Vo.setCatalog3List(catalog3VoList);
                        }
                        return catelog2Vo;
                    }).collect(Collectors.toList());
                }
                return catelog2VoList;
            }));
            String jsonString = JSON.toJSONString(collect);
            // 查到的数据放入缓存，将对象转为json
            redisTemplate.opsForValue().set("catalogJson", jsonString);
            return collect;
        }

    }


    private List<CategoryEntity> getParentCid(List<CategoryEntity> allList, Long parentCid) {
        List<CategoryEntity> collect = allList.stream().filter(item -> item.getParentCid().equals(parentCid)).collect(Collectors.toList());
        return collect;
    }


    private void recurGetPath(List<Long> paths, Long catelogId) {
        // 收集当前节点id
        CategoryEntity categoryEntity = this.getById(catelogId);
        if (categoryEntity.getParentCid() != 0) {
            this.recurGetPath(paths, categoryEntity.getParentCid());
        }
        paths.add(categoryEntity.getCatId());
    }

    public List<CategoryEntity> getChildrenList(CategoryEntity root, List<CategoryEntity> all) {
        List<CategoryEntity> childrenList = all.stream()
                .filter(item -> item.getParentCid().equals(root.getCatId()))
                .map(item -> {
                    item.setChildrenCategoryList(getChildrenList(item, all));
                    return item;
                })
                .sorted(Comparator.comparing(CategoryEntity::getSort))
                .collect(Collectors.toList());
        return childrenList;
    }

}