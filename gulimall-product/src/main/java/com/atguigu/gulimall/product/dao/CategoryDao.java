package com.atguigu.gulimall.product.dao;

import com.atguigu.gulimall.product.entity.CategoryEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 商品三级分类
 * 
 * @author zehao
 * @email 374964794@qq.com
 * @date 2021-12-18 15:01:31
 */
@Mapper
public interface CategoryDao extends BaseMapper<CategoryEntity> {
	
}
