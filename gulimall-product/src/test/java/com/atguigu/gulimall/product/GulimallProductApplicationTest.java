package com.atguigu.gulimall.product;

import com.atguigu.gulimall.product.dao.SkuSaleAttrValueDao;
import com.atguigu.gulimall.product.service.AttrGroupService;
import com.atguigu.gulimall.product.vo.SkuItemVo;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.UUID;

/**
 * @author zehao
 * @create 2021-12-17-11:30
 */
@SpringBootTest
@RunWith(SpringRunner.class)
public class GulimallProductApplicationTest {

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Autowired
    private AttrGroupService attrGroupService;

    @Autowired
    private SkuSaleAttrValueDao skuSaleAttrValueDao;

    @Test
    public void testRedis() {
        ValueOperations<String, String> opsForValue = stringRedisTemplate.opsForValue();
        opsForValue.set("hello","world_" + UUID.randomUUID().toString());
        String hello = opsForValue.get("hello");
        System.out.println(hello);
    }

    @Test
    public void testGroup() {
        attrGroupService.getAttrGroupWithAttrsBySpuId(13l, 225L);
    }

    @Test
    public void testSale () {
        List<SkuItemVo.SkuItemSaleAttrVo> saleAttrsBySpuId =
                skuSaleAttrValueDao.getSaleAttrsBySpuId(13L);
        System.out.println("ok");
    }

}