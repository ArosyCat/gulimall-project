package com.atguigu.gulimall.thirdparty.controller;

import com.atguigu.common.utils.R;
import com.atguigu.gulimall.thirdparty.component.SmsComponent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author zehao
 * @create 2022-12-23 14:53
 */
@RestController
@RequestMapping("/sms")
public class SmsSendController {

    @Autowired
    private SmsComponent smsComponent;

    /**
     * 提供给别的服务调用的，所以需要验证码，
     * 比如商品模块需要发送验证码服务，由商品模块生成好验证码后远程调用第三方服务中的发送验证码功能
     * @param phone     调用模块传入的手机号
     * @param code      调用模块传入的验证码
     */
    @GetMapping("/sendCode")
    public R sendCode(@RequestParam("phone") String phone,@RequestParam("code") String code) {
        smsComponent.sendSmsCode(phone, "code:" + code);
        return R.ok();
    }

}
