package com.atguigu.gulimall.member.exception;

/**
 * @author zehao
 * @create 2022-12-23 21:37
 */
public class PhoneExistException extends RuntimeException{

    public PhoneExistException() {
        super("手机号存在");
    }
}
