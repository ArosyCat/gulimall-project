package com.atguigu.gulimall.member.vo;

import lombok.Data;

/**
 * @author zehao
 * @create 2022-12-23 20:49
 */
@Data
public class MemberRegisterVo {

    private String userName;

    private String password;

    private String phone;

}
