package com.atguigu.gulimall.member.exception;

import com.atguigu.common.utils.R;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

/**
 * @author zehao
 * @create 2022-12-23 21:35
 */
public class UsernameExistException extends RuntimeException{

    public UsernameExistException() {
        super("用户名存在");
    }

}
