package com.atguigu.common.valid;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * @author zehao
 * @create 2022-12-02 11:06
 */
public class MyValidConstraint implements ConstraintValidator<MyValidAnnotation, Integer> {

    String regexp = "";

    @Override
    public void initialize(MyValidAnnotation constraintAnnotation) {
        regexp = constraintAnnotation.regexp();
    }

    @Override
    public boolean isValid(Integer integer, ConstraintValidatorContext constraintValidatorContext) {
        return integer.toString().matches(regexp);
    }
}
