package com.atguigu.common.to;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @author zehao
 * @create 2022-12-06 15:29
 */
@Data
public class SpuBoundTo {

    private Long spuId;
    private BigDecimal buyBounds;
    private BigDecimal growBounds;

}
