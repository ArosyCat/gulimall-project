package com.atguigu.common.constant;

/**
 * @author zehao
 * @create 2022-12-07 15:06
 */
public class WareConstant {


    public enum PurchaseDetailStatusEnum {
        //状态[0新建，1已分配，2正在采购，3已完成，4采购失败]
        CREATED(0,"新建"), ASSIGNED(1,"已分配"),
        PURCHASING(2,"正在采购"), FINISHED(3,"已完成"),
        FAILED(4,"采购失败");


        private int code;
        private String msg;

        PurchaseDetailStatusEnum(int code, String msg) {
            this.code = code;
            this.msg = msg;
        }

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMsg() {
            return msg;
        }

        public void setMsg(String msg) {
            this.msg = msg;
        }
    }

    public enum PurchaseStatusEnum {
        CREATED(0,"新建"),ASSIGNED(1,"已分配"),
        RECEIVE(2,"已领取"),FINISH(3,"已完成"),
        HASERROR(4,"有异常");

        private int code;
        private String msg;

        PurchaseStatusEnum(int code, String msg) {
            this.code = code;
            this.msg = msg;
        }

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMsg() {
            return msg;
        }

        public void setMsg(String msg) {
            this.msg = msg;
        }
    }


}
