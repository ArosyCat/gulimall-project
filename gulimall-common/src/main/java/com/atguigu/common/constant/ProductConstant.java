package com.atguigu.common.constant;

import com.sun.org.apache.xml.internal.security.Init;

/**
 * @author zehao
 * @create 2022-12-03 21:09
 */
public class ProductConstant {

    public enum productStatusEnum {

        new_spu(0,"新建"),
        spu_up(1,"商品上架"),
        spu_down(2,"商品下架");

        private int code;
        private String msg;

        productStatusEnum(int code, String msg) {
            this.code = code;
            this.msg = msg;
        }

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMsg() {
            return msg;
        }

        public void setMsg(String msg) {
            this.msg = msg;
        }
    }

    public enum AttrEnum {
        ATTR_TYPE_BASE(1,"基本属性"),
        ATTR_TYPE_SALE(0,"销售属性"),
        ATTR_TYPE_BASE_AND_SALE(2,"");

        private int code;
        private String msg;

        AttrEnum(int code, String msg) {
            this.code = code;
            this.msg = msg;
        }

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMsg() {
            return msg;
        }

        public void setMsg(String msg) {
            this.msg = msg;
        }
    }

}
