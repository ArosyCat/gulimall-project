package com.atguigu.common.exception;

/**
 * @author zehao
 * @create 2022-12-02 17:05
 */
public enum BizCodeEnum {
    UNKNOWN_EXCEPTION(10000,"系统未知异常"),
    VALID_EXCEPTION(10001,"参数格式校验失败"),
    PRODUCT_UP_EXCEPTION(10002, "商品上架异常"),
    STATUS_EXCEPTION(10003,"状态异常，无法合并已被领取的采购项"),
    SMS_CODE_EXCEPTION(10004,"验证码获取频率太高，稍后再试"),
    USER_EXIST_EXCEPTION(15001,"用户名存在"),
    PHONE_EXIST_EXCEPTION(15002,"手机号存在");
    private Integer code;
    private String message;

    BizCodeEnum(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
