package com.atguigu.gulimall.search.service;

import com.atguigu.common.to.es.SkuEsModel;

import java.io.IOException;
import java.util.List;

/**
 * @author zehao
 * @create 2022-12-10 20:32
 */
public interface ProductSaveService {

    boolean productStatusUp(List<SkuEsModel> skuEsModelList) throws IOException;
}
