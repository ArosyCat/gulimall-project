package com.atguigu.gulimall.search.vo;

import lombok.Data;

import java.util.List;

/**
 * @author zehao 封装页面所有可能传递过来的参数
 * @create 2022-12-14 16:37
 */
@Data
public class SearchParam {

    /**
     * 页面传递过来的全文匹配关键字
     */
    private String keyword;

    /**
     * 三级分类的id
     */
    private Long catalog3Id;

    /**
     * 排序条件
     * sort=saleCount_asc/desc
     * sort=skuPrice_asc/desc
     * sort=hotScore_asc/desc
     */
    private String sort;

    // 过滤条件
    // hasStock(是否有货)、skuPrice(价格区间)、brandId、catalog3Id、attrs

    /**
     * 是否只显示有货 hasStock=0/1
     */
    private Integer hasStock;

    /**
     * 价格区间 skuPrice=1_500/_500/500_
     */
    private String skuPrice;

    /**
     * 品牌
     */
    private List<Long> brandId;

    /**
     * 属性
     * attrs=2_5寸:6寸
     */
    private List<String> attrs;

    /**
     * 页码
     */
    private Integer pageNum = 1;

    /**
     * 原生的所有查询条件
     */
    private String _queryString;


}
