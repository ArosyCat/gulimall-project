package com.atguigu.gulimall.search.constant;

/**
 * @author zehao
 * @create 2022-12-10 20:35
 */
public class EsConstant {

    /**
     * Sku在es中的索引
     */
    public static final String PRODUCT_INDEX = "gulimall_product";
    public static final Integer PRODUCT_PAGESIZE = 10;
}
