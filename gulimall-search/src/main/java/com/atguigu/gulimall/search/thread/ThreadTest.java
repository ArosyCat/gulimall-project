package com.atguigu.gulimall.search.thread;

import javax.security.auth.login.Configuration;
import java.util.concurrent.*;

/**
 * 创建线程的四种方式
 * 1. 继承 Thread new Thread().start()
 *
 * 2. 实现Runnable 接口 new Thread(new Runnable01()).start()
 *
 * 3. 实现Callable 接口 + FutureTask  new Thread(new FutureTask(new Callable01())).start
 *
 * 4. 线程池 [ExecutorService]
 *      给线程池直接提交任务
 *      service.execute(new Runnable01());
 *   1. 创建：
 *      1）、Executors
 *      2）、new ThreadPoolExecutor
 *  区别：
 *      1、2不能得到返回值，3可以得到返回值
 *      1、2、3都不能控制资源
 *      4可以控制资源，性能稳定
 *
 *  在业务代码中，以上三种启动线程的方式都不用。【将所有的多线程异步任务都交给线程池执行】
 *
 *  线程池的七大参数：
 *  1、corePoolSize：核心线程数【一直存在，除非设置了(allowCoreThreadTimeOut)】，线程池创建好以后就准备就绪的线程数量，等待来接收异步任务去执行。
 *      new 了5个Thread    thread.start();
 *  2、maximumPoolSize：最大线程数，控制资源并发
 *  3、keepAliveTime：存活时间。如果当前的线程数量大于core数量
 *          释放空闲的线程（释放 maximumPoolSize - corePoolSize），只要线程空闲大于指定的空闲时间（keepAliveTime）
 *  4、unit：时间单位
 *  5、BlockingQueue：阻塞队列，如果任务有很多，就会将目前多的任务放在队列里。
 *          只要有线程空闲，就会去队列中取出新的任务执行
 *  6、threadFactory：线程的创建工厂
 *  7、handler：拒绝策略。如果队列满了，无法存下更多的任务，按照我们指定的拒绝策略，拒绝执行其他任务。
 *
 *  工作顺序：
 *  1. 线程池创建，准备好core数量的核心线程，准备接受任务
 *      1.1 core满了，就将再进来的任务放入阻塞队列中，空闲的core就会自己去阻塞队列获取任务执行
 *      1.2 阻塞队列满了，就直接开新线程执行，最大只能开到max指定的数量
 *      1.3 max满了，就用RejectedExecutionHandler 拒绝任务
 *      1.4 max都执行完成，有很多空闲，在指定的时间 keepAliveTime以后，释放max-core这些线程
 *          （即让线程尽量保持在设置的 core核心线程数上）
 *
 * 面试题：
 * 一个线程池 core-7; max-20; queue-50，100并发进来会怎么分配？
 * （1）7个会立即得到执行
 * （2）50个会进入队列
 * （3）再开13个进行执行
 * （4）拒绝掉30个
 *  如果不想抛弃还要执行，可以指定拒绝策略
 *
 *  Executors.newCachedThreadPool();        core是0，所有都可回收
 *  Executors.newFixedThreadPool();         固定大小，core=max，都不可回收
 *  Executors.newScheduledThreadPool();     定时任务的线程池
 *  Executors.newSingleThreadExecutor();    单线程的线程池，后台从队列里面获取任务，挨个执行
 *
 * @author zehao
 * @create 2022-12-18 15:37
 */
public class ThreadTest {

    public static ExecutorService service = Executors.newFixedThreadPool(10);

    public static void main(String[] args) throws ExecutionException, InterruptedException {
        System.out.println("main...start...");
//        CompletableFuture<Void> completableFuture = CompletableFuture.runAsync(() -> {
//            System.out.println("当前线程 = " + Thread.currentThread().getId());
//            int i = 10 / 2;
//            System.out.println("运行结果 = " + i);
//        }, service);
        CompletableFuture<Integer> completableFuture = CompletableFuture.supplyAsync(() -> {
            System.out.println("当前线程 = " + Thread.currentThread().getId());
            int i = 10 / 0;
            System.out.println("运行结果 = " + i);
            return i;
        }, service).whenComplete((res,ex) -> {
            // 虽然能得到异常信息，但是没法修改返回数据
            System.out.println("异步任务成功完成了，结果是：" + res + "异常是：" + ex);
        }).exceptionally(throwable -> {
            // 可以感知异常，同时返回默认值
            return 10;
        });
        Integer integer = completableFuture.get();
        System.out.println("main...end..." + integer);
    }

    public static class runnable01 implements Runnable {

        @Override
        public void run() {

        }
    }

    public static class Callable01 implements Callable<Integer> {
        @Override
        public Integer call() throws Exception {
            int i  = 10;
            System.out.println("启动线程callable01...");
            return i - 5;
        }
    }

}
