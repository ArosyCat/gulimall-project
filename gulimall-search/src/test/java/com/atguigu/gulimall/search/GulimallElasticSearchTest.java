package com.atguigu.gulimall.search;

import com.alibaba.fastjson.JSON;
import com.atguigu.gulimall.search.config.GulimallElasticSearchConfig;
import lombok.Data;
import lombok.ToString;
import org.elasticsearch.action.get.GetRequest;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.aggregations.Aggregation;
import org.elasticsearch.search.aggregations.AggregationBuilder;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.bucket.terms.TermsAggregationBuilder;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runner.Runner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.security.SecureRandom;
import java.util.Arrays;
import java.util.Map;

/**
 * @author zehao
 * @create 2022-12-09 14:46
 */
@SpringBootTest
@RunWith(SpringRunner.class)
public class GulimallElasticSearchTest {

    @Autowired
    private RestHighLevelClient client;

    @Test
    public void testString() {
        String str = "_500";
        String str1 = "500_";
        String[] s = str.split("_");
        String[] s1 = str1.split("_");
        System.out.println(Arrays.asList(s));
        System.out.println(s.length);
        System.out.println(Arrays.asList(s1));
        System.out.println(s1.length);
    }

    @Data
    @ToString
    static class Account {
        private int account_number;
        private int balance;
        private String firstname;
        private String lastname;
        private int age;
        private String gender;
        private String address;
        private String employer;
        private String email;
        private String city;
        private String state;
    }

    @Test
    public void searchDate () throws IOException {
        // 1. 创建检索请求，指定索引
        SearchRequest searchRequest = new SearchRequest("bank");
        // 3. 检索条件
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        searchSourceBuilder.query(QueryBuilders.matchAllQuery());
        TermsAggregationBuilder ageAgg = AggregationBuilders.terms("ageAgg").field("age").size(100);
        TermsAggregationBuilder genderAgg = AggregationBuilders.terms("genderAgg").field("gender.keyword");
        genderAgg.subAggregation(AggregationBuilders.avg("genderBalance").field("balance"));
        ageAgg.subAggregation(genderAgg);
        ageAgg.subAggregation(AggregationBuilders.avg("totalBalance").field("balance"));
        searchSourceBuilder.aggregation(ageAgg);
        // 2. 指定DSL，检索条件
        searchRequest.source(searchSourceBuilder);
        SearchResponse search = client.search(searchRequest, GulimallElasticSearchConfig.COMMON_OPTIONS);
        for (SearchHit hit : search.getHits().getHits()) {
            String jsonString = hit.getSourceAsString();
            Account account = JSON.parseObject(jsonString, Account.class);
            System.out.println(account);
        }

    }

    /**
     * 测试存储数据
     */
    @Test
    public void indexData () {
        IndexRequest indexRequest = new IndexRequest("users");
        indexRequest.id("1");
        User user = new User();
        user.setUserName("张三");
        user.setAge(18);
        user.setGender("男");
        String jsonString = JSON.toJSONString(user);
        indexRequest.source(jsonString, XContentType.JSON); // 要保存的内容
        try {
            IndexResponse index = client.index(indexRequest, GulimallElasticSearchConfig.COMMON_OPTIONS);
            // 提取有用的响应数据
            System.out.println(index);
        } catch (IOException e) {
        }
    }

    @Data
    class User {
        private String userName;
        private String gender;
        private Integer age;
    }

    @Test
    public void getClient () {
        GetRequest request = new GetRequest("bank","1");
        try {
            GetResponse response = client.get(request, GulimallElasticSearchConfig.COMMON_OPTIONS);
            System.out.println(response);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

}
