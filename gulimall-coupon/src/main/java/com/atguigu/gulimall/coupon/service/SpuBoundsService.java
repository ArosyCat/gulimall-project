package com.atguigu.gulimall.coupon.service;

import com.atguigu.common.to.SpuBoundTo;
import com.baomidou.mybatisplus.extension.service.IService;
import com.atguigu.common.utils.PageUtils;
import com.atguigu.gulimall.coupon.entity.SpuBoundsEntity;

import java.util.Map;

/**
 * 商品spu积分设置
 *
 * @author zehao
 * @email 374964794@qq.com
 * @date 2021-12-18 17:02:59
 */
public interface SpuBoundsService extends IService<SpuBoundsEntity> {

    PageUtils queryPage(Map<String, Object> params);

    void saveBound(SpuBoundTo spuBoundTo);
}

