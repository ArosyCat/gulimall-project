package com.atguigu.gulimall.coupon.dao;

import com.atguigu.gulimall.coupon.entity.SkuBoundsEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 商品sku积分设置
 * 
 * @author zehao
 * @email 374964794@qq.com
 * @date 2021-12-18 17:03:00
 */
@Mapper
public interface SkuBoundsDao extends BaseMapper<SkuBoundsEntity> {
	
}
