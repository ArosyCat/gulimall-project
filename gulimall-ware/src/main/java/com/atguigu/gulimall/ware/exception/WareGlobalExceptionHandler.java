package com.atguigu.gulimall.ware.exception;

import com.atguigu.common.exception.BizCodeEnum;
import com.atguigu.common.utils.R;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * @author zehao
 * @create 2022-12-07 16:40
 */
@Slf4j
@RestControllerAdvice(basePackages = {"com.atguigu.gulimall.ware.controller"})
public class WareGlobalExceptionHandler {

    @ExceptionHandler(StatusExecption.class)
    public R handleStatusException(StatusExecption ex) {
        ex.printStackTrace();
        log.error("出现状态异常：{}, 异常类型：{}", ex.getMessage(), ex.getClass());
        return R.error(BizCodeEnum.STATUS_EXCEPTION.getCode(), BizCodeEnum.STATUS_EXCEPTION.getMessage());
    }

}
