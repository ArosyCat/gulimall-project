package com.atguigu.gulimall.ware.exception;

/**
 * @author zehao
 * @create 2022-12-07 16:41
 */
public class StatusExecption extends RuntimeException{

    private static final long serialVersionUID = 1L;

    public StatusExecption() {
        super();
    }

    public StatusExecption(String s) {
        super(s);
    }

}
