package com.atguigu.gulimall.ware.service;

import com.atguigu.gulimall.ware.vo.DoneVo;
import com.baomidou.mybatisplus.extension.service.IService;
import com.atguigu.common.utils.PageUtils;
import com.atguigu.gulimall.ware.entity.WareSkuEntity;

import java.util.List;
import java.util.Map;

/**
 * 商品库存
 *
 * @author zehao
 * @email 374964794@qq.com
 * @date 2021-12-18 17:38:48
 */
public interface WareSkuService extends IService<WareSkuEntity> {

    PageUtils queryPage(Map<String, Object> params);


    void addStock(Long skuId, Long wareId, Integer skuNum);

    Map<Long, Boolean> getSkuHasStock(List<Long> skuIds);
}

