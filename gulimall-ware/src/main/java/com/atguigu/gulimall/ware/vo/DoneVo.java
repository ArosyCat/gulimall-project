package com.atguigu.gulimall.ware.vo;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @author zehao
 * @create 2022-12-07 17:30
 */
@Data
public class DoneVo {

    @NotNull
    private Long id; // 采购单id
    private List<PurchaseDetailVo> items;

}
