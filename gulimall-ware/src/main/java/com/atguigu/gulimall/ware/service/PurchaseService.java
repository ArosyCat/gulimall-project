package com.atguigu.gulimall.ware.service;

import com.atguigu.gulimall.ware.vo.DoneVo;
import com.atguigu.gulimall.ware.vo.MergeVo;
import com.baomidou.mybatisplus.extension.service.IService;
import com.atguigu.common.utils.PageUtils;
import com.atguigu.gulimall.ware.entity.PurchaseEntity;

import java.util.Map;

/**
 * 采购信息
 *
 * @author zehao
 * @email 374964794@qq.com
 * @date 2021-12-18 17:38:48
 */
public interface PurchaseService extends IService<PurchaseEntity> {

    PageUtils queryPage(Map<String, Object> params);

    PageUtils getUnReceiveList(Map<String, Object> params);

    void mergePurchase(MergeVo purchaseVo);

    void receivedPurchase(Long[] purchaseIds);

    void purchaseDone(DoneVo doneVo);
}

