package com.atguigu.gulimall.ware.service.impl;

import com.atguigu.common.constant.WareConstant;
import com.atguigu.common.utils.R;
import com.atguigu.gulimall.ware.entity.PurchaseDetailEntity;
import com.atguigu.gulimall.ware.exception.StatusExecption;
import com.atguigu.gulimall.ware.feign.ProductFeignService;
import com.atguigu.gulimall.ware.service.PurchaseDetailService;
import com.atguigu.gulimall.ware.service.WareSkuService;
import com.atguigu.gulimall.ware.vo.DoneVo;
import com.atguigu.gulimall.ware.vo.MergeVo;
import com.atguigu.gulimall.ware.vo.PurchaseDetailVo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.atguigu.common.utils.PageUtils;
import com.atguigu.common.utils.Query;

import com.atguigu.gulimall.ware.dao.PurchaseDao;
import com.atguigu.gulimall.ware.entity.PurchaseEntity;
import com.atguigu.gulimall.ware.service.PurchaseService;
import org.springframework.transaction.annotation.Transactional;


@Service("purchaseService")
public class PurchaseServiceImpl extends ServiceImpl<PurchaseDao, PurchaseEntity> implements PurchaseService {

    @Autowired
    private PurchaseDetailService purchaseDetailService;

    @Autowired
    private ProductFeignService productFeignService;

    @Autowired
    private WareSkuService wareSkuService;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<PurchaseEntity> page = this.page(
                new Query<PurchaseEntity>().getPage(params),
                new QueryWrapper<PurchaseEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public PageUtils getUnReceiveList(Map<String, Object> params) {
        QueryWrapper<PurchaseEntity> wrapper = new QueryWrapper<>();
        wrapper.eq("status",0).or().eq("status", 1);
        IPage<PurchaseEntity> page = this.page(new Query<PurchaseEntity>().getPage(params), wrapper);
        return new PageUtils(page);
    }

    @Transactional(rollbackFor = {Exception.class})
    @Override
    public void mergePurchase(MergeVo purchaseVo) {
        Long purchaseId = purchaseVo.getPurchaseId();
        if (purchaseId == null) {
            // 新建
            PurchaseEntity purchaseEntity = new PurchaseEntity();
            purchaseEntity.setCreateTime(new Date());
            purchaseEntity.setStatus(WareConstant.PurchaseStatusEnum.CREATED.getCode());
            this.save(purchaseEntity);
            purchaseId = purchaseEntity.getId();
        }

        for (Long item : purchaseVo.getItems()) {
            PurchaseDetailEntity detailServiceById = purchaseDetailService.getById(item);
            if (!(detailServiceById.getStatus() == WareConstant.PurchaseDetailStatusEnum.CREATED.getCode() ||
                    detailServiceById.getStatus() == WareConstant.PurchaseDetailStatusEnum.ASSIGNED.getCode())) {
                throw new StatusExecption();
            }
        }

        // Variable used in lambda expression should be final or effectively final
        Long finalPurchaseId = purchaseId;
        List<PurchaseDetailEntity> detailEntities = purchaseVo.getItems().stream().map(item -> {
            PurchaseDetailEntity purchaseDetailEntity = new PurchaseDetailEntity();
            purchaseDetailEntity.setId(item);
            purchaseDetailEntity.setPurchaseId(finalPurchaseId);
            purchaseDetailEntity.setStatus(WareConstant.PurchaseDetailStatusEnum.ASSIGNED.getCode());
            return purchaseDetailEntity;
        }).collect(Collectors.toList());
        purchaseDetailService.updateBatchById(detailEntities);
        PurchaseEntity purchaseEntity = new PurchaseEntity();
        purchaseEntity.setId(purchaseId);
        purchaseEntity.setUpdateTime(new Date());
        this.updateById(purchaseEntity);
    }

    /**
     * 完成采购任务
     * @param doneVo  采购单返回视图对象
     */
    @Transactional(rollbackFor = {Exception.class})
    @Override
    public void purchaseDone(DoneVo doneVo) {

        Long id = doneVo.getId();
        // 2. 改变采购项状态
        Boolean flag = true;
        List<PurchaseDetailEntity> updateItemList = new ArrayList<>();
        for (PurchaseDetailVo item : doneVo.getItems()) {
            PurchaseDetailEntity purchaseDetailEntity = new PurchaseDetailEntity();
            if (item.getStatus() == WareConstant.PurchaseDetailStatusEnum.FAILED.getCode()) {
                flag = false;
                purchaseDetailEntity.setStatus(WareConstant.PurchaseDetailStatusEnum.FAILED.getCode());
            } else {
                purchaseDetailEntity.setStatus(WareConstant.PurchaseDetailStatusEnum.FINISHED.getCode());
                // 将成功采购的进行入库
                PurchaseDetailEntity detailEntity = purchaseDetailService.getById(item.getItemId());
                Long skuId = detailEntity.getSkuId();
                Long wareId = detailEntity.getWareId();
                Integer skuNum = detailEntity.getSkuNum();
                wareSkuService.addStock(skuId,wareId,skuNum);
            }
            purchaseDetailEntity.setId(item.getItemId());
            updateItemList.add(purchaseDetailEntity);
        }
        purchaseDetailService.updateBatchById(updateItemList);
        // 1. 改变采购单状态
        PurchaseEntity purchaseEntity = new PurchaseEntity();
        if (!flag) {
            purchaseEntity.setStatus(WareConstant.PurchaseStatusEnum.HASERROR.getCode());
        } else {
            purchaseEntity.setStatus(WareConstant.PurchaseStatusEnum.FINISH.getCode());
        }
        purchaseEntity.setId(id);
        this.updateById(purchaseEntity);
        // 3. 将成功采购的进行入库



    }

    /**
     * 采购人员领取采购单
     * @param purchaseIds 采购单id
     */
    @Transactional(rollbackFor = {Exception.class})
    @Override
    public void receivedPurchase(Long[] purchaseIds) {
        // 确认当前采购单是新建或已分配状态，才可以领取
        List<PurchaseEntity> entities = Arrays.stream(purchaseIds).map(item -> {
            return this.getById(item);
        }).filter(item -> {
            boolean legalStatus = item.getStatus() == WareConstant.PurchaseStatusEnum.CREATED.getCode() || item.getStatus() == WareConstant.PurchaseStatusEnum.ASSIGNED.getCode();
           if (!Objects.isNull(item) && legalStatus) {
               return true;
           } else {
               return false;
           }
        }).collect(Collectors.toList());
        if (!Objects.isNull(entities) && entities.size() > 0) {
            List<PurchaseEntity> entityList = entities.stream().map(item -> {
                item.setUpdateTime(new Date());
                item.setStatus(WareConstant.PurchaseStatusEnum.RECEIVE.getCode());
                // 修改采购需求状态
                List<PurchaseDetailEntity> purchaseDetailEntities = purchaseDetailService.list(new QueryWrapper<PurchaseDetailEntity>()
                        .eq("purchase_id", item.getId()));
                if (!Objects.isNull(purchaseDetailEntities) && purchaseDetailEntities.size() > 0) {
                    List<PurchaseDetailEntity> detailEntityList = purchaseDetailEntities.stream().map(entity -> {
                        entity.setStatus(WareConstant.PurchaseDetailStatusEnum.PURCHASING.getCode());
                        return entity;
                    }).collect(Collectors.toList());
                    purchaseDetailService.updateBatchById(detailEntityList);
                }
                return item;
            }).collect(Collectors.toList());
            this.updateBatchById(entityList);
        }

    }
}