package com.atguigu.gulimall.ware.vo;

import lombok.Data;

import java.util.List;

/**
 * @author zehao
 * @create 2022-12-07 10:59
 */
@Data
public class MergeVo {

    private Long purchaseId;
    private List<Long> items;

}
