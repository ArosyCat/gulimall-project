package com.atguigu.gulimall.ware.vo;

import lombok.Data;

/**
 * @author zehao
 * @create 2022-12-07 21:16
 */
@Data
public class PurchaseDetailVo {
    private Long itemId;
    private Integer status;
    private String reason;
}
